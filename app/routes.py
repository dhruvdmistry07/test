from app import app
from db.mongodb import MongoDB
db_instance = MongoDB()

@app.route('/')
@app.route('/index')
@app.route('/home')
def index():
    data = {
                "first_name": "Harnisha",
                "last_name": "Mistry",
                "email": "hanimistry@mail.com",
            }
    db_instance.save_data_mongodb(data)
    return "Data has been saved successfully!"
